package v2_1_1.core

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import org.koin.ktor.ext.inject
import persistence.OCPIPartner
import v2_1_1.model.credentials.OCPICredentials

@KtorExperimentalAPI
fun Route.v2_1_1_credentials() {
    val credentialsService: CredentialsService by inject()

    post("/2.1.1/credentials") {
        val credentials = call.receive<OCPICredentials>()
        val partner = call.principal<OCPIPartner>() ?: throw IllegalStateException()
        val handshakeCredentials = credentialsService.performHandshake(partner, credentials)
        call.respond(handshakeCredentials)
    }
}
