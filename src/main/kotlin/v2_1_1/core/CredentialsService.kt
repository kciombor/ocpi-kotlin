package v2_1_1.core

import config.rest.OCPIClient
import io.ktor.util.*
import persistence.OCPIPartner
import v2_1_1.model.credentials.OCPICredentials
import v2_1_1.model.locations.OCPIBusinessDetails
import versions.OCPIVersionNumber
import java.util.*

@KtorExperimentalAPI
class CredentialsService {
    suspend fun performHandshake(partner: OCPIPartner, credentials: OCPICredentials): OCPICredentials {
        val ocpiClient = OCPIClient(credentials.token)

        val version = ocpiClient.getVersion(credentials.url, OCPIVersionNumber.v2_1_1)
        val versionDetails = ocpiClient.getVersionDetails(version.url)

        val tokenC = UUID.randomUUID().toString()

        return OCPICredentials(
            token = tokenC,
            url = "http://localhost:8080",
            businessDetails = OCPIBusinessDetails(
                name = "Test OCPI",
            ),
            partyId = "TST",
            countryCode = "PL"
        )
    }
}
