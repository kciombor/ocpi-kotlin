package v2_1_1.cpo

import common.OCPIResponse
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.cpoLocations() {
    get("/locations") {
        call.respond(
            OCPIResponse.success("LOCATIONS")
        )
    }
}
