package v2_1_1.model.credentials

import v2_1_1.model.OCPIModel
import v2_1_1.model.locations.OCPIBusinessDetails

data class OCPICredentials(
    val token: String,
    val url: String,
    val businessDetails: OCPIBusinessDetails,
    val partyId: String,
    val countryCode: String,
) : OCPIModel()
