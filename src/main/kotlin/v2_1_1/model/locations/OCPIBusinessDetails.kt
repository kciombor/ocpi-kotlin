package v2_1_1.model.locations

import v2_1_1.model.OCPIModel

data class OCPIBusinessDetails(
    val name: String,
    val website: String? = null,
    val logo: OCPIImage? = null,
) : OCPIModel()
