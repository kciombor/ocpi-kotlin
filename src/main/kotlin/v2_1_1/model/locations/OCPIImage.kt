package v2_1_1.model.locations

import v2_1_1.model.OCPIModel

data class OCPIImage(
    val url: String,
    val thumbnail: String?,
    val category: OCPIImageCategory,
    val type: String,
    val width: Number?,
    val height: Number?,
) : OCPIModel()
