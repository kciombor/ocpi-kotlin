package v2_1_1.model.locations

import v2_1_1.model.OCPIModel

data class OCPILocation(
    val id: String,
    val type: String,
    val name: String?,
    val address: String,
    val city: String,
    val postalCode: String,
    val country: String
) : OCPIModel()
