package v2_1_1.emsp

import common.OCPIResponse
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import v2_1_1.model.locations.OCPILocation

fun Route.emspLocations() {
    put("/locations") {
        val location = call.receive<OCPILocation>()
        call.respond(OCPIResponse.success<Any>())
    }
}
