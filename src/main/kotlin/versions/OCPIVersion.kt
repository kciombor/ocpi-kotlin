package versions

data class OCPIVersion(
    val version: String,
    val url: String
)
