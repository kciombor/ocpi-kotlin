package versions

data class OCPIEndpoint(
    val url: String,
    val identifier: OCPIModuleID
)
