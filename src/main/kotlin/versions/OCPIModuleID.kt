package versions

enum class OCPIModuleID(val value: String) {
    CDRs("cdrs"),
    Commands("commands"),
    Credentials("credentials"),
    Locations("locations"),
    Sessions("sessions"),
    Tariffs("tariffs"),
    Tokens("Tokens")
}
