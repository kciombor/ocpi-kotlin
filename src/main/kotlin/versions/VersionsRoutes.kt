package versions

import common.OCPIResponse
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.versions() {
    get("/versions") {
        call.respond(
            OCPIResponse.success(
                listOf(
                    OCPIVersion(
                        version = "2.1.1",
                        url = "http://localhost:8080/ocpi/2.1.1"
                    )
                )
            )
        )
    }

    get("/2.1.1") {
        call.respond(
            OCPIResponse.success(
                OCPIVersionDetails(
                    version = OCPIVersionNumber.v2_1_1,
                    endpoints = listOf(
                        OCPIEndpoint(
                            url = "http://localhost:8080/ocpi/emsp/2.1.1/locations",
                            identifier = OCPIModuleID.Locations,
                        )
                    )
                )
            )
        )
    }
}
