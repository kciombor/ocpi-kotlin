package versions

enum class OCPIVersionNumber(val value: String) {
    v2_0("2.0"),
    v2_1_1("2.1.1"),
    v2_2("2.2")
}
