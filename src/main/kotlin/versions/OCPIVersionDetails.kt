package versions

data class OCPIVersionDetails(
    val version: OCPIVersionNumber,
    val endpoints: Collection<OCPIEndpoint>,
)
