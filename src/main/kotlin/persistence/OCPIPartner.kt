package persistence

import io.ktor.auth.*

data class OCPIPartner(
    val partnerName: String,
    val partnerSideToken: String
) : Principal
