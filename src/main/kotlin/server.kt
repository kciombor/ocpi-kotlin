import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import common.OCPIResponse
import config.ocpiAppModule
import config.security.OCPINonRegisteredValidationStrategy
import config.security.OCPIRegisteredValidationStrategy
import config.security.ocpiAuth
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.*
import org.koin.ktor.ext.Koin
import v2_1_1.core.v2_1_1_credentials
import v2_1_1.cpo.cpoLocations
import v2_1_1.emsp.emspLocations
import versions.versions

@KtorExperimentalAPI
fun main() {
    embeddedServer(Netty, port = 8080) {
        install(Authentication) {
            ocpiAuth("ocpi-non-registered") {
                validate { token -> OCPINonRegisteredValidationStrategy().validate(token) }
            }
            ocpiAuth("ocpi-registered") {
                validate { token -> OCPIRegisteredValidationStrategy().validate(token) }
            }
        }

        install(CallLogging)

        install(ContentNegotiation) {
            jackson {
                registerModule(KotlinModule())
                registerModule(JavaTimeModule())
                disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            }
        }

        install(StatusPages) {
            exception<Throwable> { exception ->
                call.application.environment.log.warn("Uncaught exception", exception)
                call.respond(HttpStatusCode.OK, OCPIResponse.failure(3000, "Something went wrong"))
            }
        }

        install(Koin) {
            modules(ocpiAppModule)
        }

        routing {
            route("/ocpi") {
                authenticate("ocpi-non-registered") {
                    versions()
                    v2_1_1_credentials()
                }

                authenticate("ocpi-registered") {
                    route("/cpo") {
                        // CPO routes
                        cpoLocations()
                    }
                    route("/emsp") {
                        // eMSPs routes
                        emspLocations()
                    }
                }
            }
        }
    }.start(wait = true)
}
