package config

import org.koin.dsl.module
import v2_1_1.core.CredentialsService

val ocpiAppModule = module(createdAtStart = true) {
    single { CredentialsService() }
}
