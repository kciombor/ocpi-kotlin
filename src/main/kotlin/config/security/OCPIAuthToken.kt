package config.security

import io.ktor.auth.*
import io.ktor.http.auth.*
import io.ktor.request.*

data class OCPIAuthToken(
    val token: String
)

fun ApplicationRequest.ocpiAuthToken(): OCPIAuthToken? {
    when (val authHeader = parseAuthorizationHeader()) {
        is HttpAuthHeader.Single -> {
            if (!authHeader.authScheme.equals("Token", ignoreCase = true)) {
                return null
            }
            return OCPIAuthToken(authHeader.blob)
        }
        else -> return null
    }
}
