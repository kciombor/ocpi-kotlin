package config.security

import io.ktor.auth.*
import persistence.OCPIPartner

class OCPINonRegisteredValidationStrategy : OCPITokenValidationStrategy {
    override fun validate(token: OCPIAuthToken): Principal? {
        return if (token.token == "non-registered") OCPIPartner("Non-registered test", "non-registered") else null
    }
}
