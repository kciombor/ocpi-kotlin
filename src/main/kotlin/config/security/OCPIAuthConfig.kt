package config.security

import common.OCPIResponse
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*

fun Authentication.Configuration.ocpiAuth(
    name: String? = null,
    configure: OCPIAuthProvider.Configuration.() -> Unit
) {
    val provider = OCPIAuthProvider(OCPIAuthProvider.Configuration(name).apply(configure))
    val authenticate = provider.authenticationFunction

    provider.pipeline.intercept(AuthenticationPipeline.RequestAuthentication) { context ->
        val token = call.request.ocpiAuthToken()
        val principal = token?.let { authenticate(call, it) }

        val cause = when {
            token == null -> AuthenticationFailedCause.NoCredentials
            principal == null -> AuthenticationFailedCause.InvalidCredentials
            else -> null
        }

        if (cause != null) {
            context.challenge(ocpiAuthenticationChallengeKey, cause) {
                call.respond(HttpStatusCode.Unauthorized, OCPIResponse.failure(2000, causeToMessage(cause)))
                it.complete()
            }
        }
        if (principal != null) {
            context.principal(principal)
        }
    }
    register(provider)
}

fun causeToMessage(cause: AuthenticationFailedCause): String {
    return when (cause) {
        AuthenticationFailedCause.NoCredentials -> "Missing auth token header"
        AuthenticationFailedCause.InvalidCredentials -> "Token not found"
        else -> "Error during authorization"
    }
}

private const val ocpiAuthenticationChallengeKey = "OCPIAuth"
