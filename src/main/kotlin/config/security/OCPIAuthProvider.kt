package config.security

import io.ktor.application.*
import io.ktor.auth.*

class OCPIAuthProvider(
    configuration: Configuration
) : AuthenticationProvider(configuration) {
    internal val authenticationFunction = configuration.authenticationFunction

    class Configuration(name: String?) : AuthenticationProvider.Configuration(name) {
        var authenticationFunction: AuthenticationFunction<OCPIAuthToken> = {
            throw NotImplementedError(
                "OCPI auth validate function is not specified. Use ocpiAuth { validate { ... } } to fix."
            )
        }

        fun validate(body: suspend ApplicationCall.(OCPIAuthToken) -> Principal?) {
            authenticationFunction = body
        }
    }
}
