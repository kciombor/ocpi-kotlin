package config.security

import io.ktor.auth.*

interface OCPITokenValidationStrategy {
    fun validate(token: OCPIAuthToken): Principal?
}
