package config.security

import io.ktor.auth.*
import persistence.OCPIPartner

class OCPIRegisteredValidationStrategy : OCPITokenValidationStrategy {
    override fun validate(token: OCPIAuthToken): Principal? {
        return if (token.token == "registered") OCPIPartner("Registered test", "registered") else null
    }
}
