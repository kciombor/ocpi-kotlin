package config.rest

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import common.OCPIResponse
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import versions.OCPIVersion
import versions.OCPIVersionDetails
import versions.OCPIVersionNumber

@KtorExperimentalAPI
class OCPIClient(private val authToken: String) {
    private val restClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = JacksonSerializer {
                propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
                registerModule(JavaTimeModule())
            }
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.HEADERS
        }
        defaultRequest {
            header(HttpHeaders.Authorization, "Token $authToken")
        }
    }

    suspend fun getVersion(url: String, requiredVersion: OCPIVersionNumber): OCPIVersion {
        val versionsResponse = restClient.get<OCPIResponse<Collection<OCPIVersion>>>(url)

        if (versionsResponse.data == null) {
            throw RuntimeException("Invalid response")
        }

        return versionsResponse.data
            .first { version -> version.version == requiredVersion.value }
    }

    suspend fun getVersionDetails(url: String): OCPIVersionDetails? {
        val versionDetailsResponse = restClient.get<OCPIResponse<OCPIVersionDetails>>(url)

        if (versionDetailsResponse.data == null) {
            throw RuntimeException("Invalid response")
        }

        return versionDetailsResponse.data
    }
}
