package common

import java.time.OffsetDateTime
import java.time.ZoneOffset

data class OCPIResponse<T>(
    val data: T?,
    val status: Int,
    val message: String,
    val timestamp: OffsetDateTime
) {
    companion object {
        fun <R> success(data: R? = null): OCPIResponse<R> {
            return OCPIResponse(
                data = data,
                status = 1000,
                message = "Success",
                timestamp = OffsetDateTime.now(ZoneOffset.UTC)
            )
        }

        fun failure(status: Int, message: String): OCPIResponse<*> {
            return OCPIResponse(
                data = null,
                status = status,
                message = message,
                timestamp = OffsetDateTime.now(ZoneOffset.UTC)
            )
        }
    }
}
